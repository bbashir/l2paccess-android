# L2P Access Android Project #

The aim of this project is to give the development of Android applications for RWTH Aachen University's L²P online teaching and learning platform a jump start. This project provides the Authentication process along with some added goodies needed to make calls to the L²P API.  This Authentication not only includes getting the user code and verification of it but also fetching and refreshing of the access token which is to be used when calling the L²P API.  When using this project you quickly start to develop your own new innovative and awesome without having to spend too much time worrying about authenticating your application.

[TOC]

##Prerequisite##
Make sure that you have registered yourself as a developer with the ITC RWTH Aachen and received a client Id from them, without the client Id you will not be able to access the L2P API. Place the client Id in a text file with the name "apiclientID.txt", add this eventually to the assets folder of your application. 

##Usage##

###Add To Project###
*Important*: This guide contains steps to include the L2PAccess-Android project in the Android Studio IDE projects.

####Step 1 - Download the Project ####

In order to add the L2PAccess Android project into your own project, you need to either pull it to your local fork of the repository or download it as an archive file. This guide will follow the path the downloading the source code as an archive file. To download the latest version of the project follow this [link](https://bitbucket.org/bbashir/l2paccess-android/get/master.zip) and download the latest contents of the master branch. Extract the contents of the downloaded archive file into a local folder.

####Step 2 - Import Project ####
Now that you have the downloaded the project, let's add it to your own Android Application Project. 

1. Go to **File** → **Import Module**, enter the location of the folder where you extracted the project contents

![Import_Module_SC.jpg](https://bitbucket.org/repo/X9j5R5/images/3344874654-Import_Module_SC.jpg)

2. Finish the process of importing the module

You should now be able see the source code of the project as a new module in your project window.

####Step 3 - Adding the New Module as a Dependency####
1. Navigate to the Module settings of your project's module, as shown below

![Module_Settings.jpg](https://bitbucket.org/repo/X9j5R5/images/879896748-Module_Settings.jpg)

2. Go to the Dependency tab of your project's module and add the *l2paccess* module to the list of dependencies 

![Module_Dependency.jpg](https://bitbucket.org/repo/X9j5R5/images/543799023-Module_Dependency.jpg)

Great, now you should be able to access the classes of the L2PAccess module from within your module. We're done with the setup! Now lets move on to the more interesting stuff, how to use the L2PAccess module.

###Using L2PAccess###
The L2PAccess project is provided as a Library Module, which means it has its own manifest file etc. more information on the structure of a Library Module can be found [here](https://developer.android.com/tools/projects/index.html#LibraryModules). Using the L2PAccess is simple and should not cause you much grief. The core of the project is an Android Service which goes by the name *OAuthTokenService*. This service is responsible for carrying out the whole process of Authentication and making sure that access token is kept up to date. All you need to do is bind to this Service from your Activity. Since the *OAuthTokenService* is a bound service it only lives until a context is bound to it. For  more information on Bound Services refer to this [link](http://developer.android.com/guide/components/bound-services.html). After binding to the *OAuthTokenService* simply call the *getL2PAccessToken()* method to get the actual access token to access the L2P API. 

If you bind to the *OAuthTokenService* for the first time and your application has not been authenticated as yet, then the service will fire off the *OAuthActivity*. This Activity carries out the initial step of authenticating the application till the point that the Refresh and Access tokens are retrieved. 

A sample Activity “L2PAccessActivity” is also provided as part of the project to illustrate how to bind to the Service and procedure to retrieve the access token from it.

**Note:** Make sure to include the *OAuthTokenService* as a service  and the *OAuthActivity* as an Activity in your module's manifest file.

## Architecture Overview ##
![architecture_overview.jpg](https://bitbucket.org/repo/X9j5R5/images/285523512-architecture_overview.jpg)

## Issues? Who do I talk to? ##
If you find any issues or want to suggest improvements, do report them on this [page](https://bitbucket.org/bbashir/l2paccess-android/issues). Or even better feel free to modify/fix them yourself and merge your changes into the master. 
As this project is something that I will not be actively maintaining it could be a while before you get a reply from me.

Happy Coding!